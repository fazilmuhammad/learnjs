console.log (1 === 1); // expected output: false
console.log ('1' === '1');  // expected output: false
console.log (1 === '1'); // expected output: false
console.log ('1' === 1);  // expected output: false