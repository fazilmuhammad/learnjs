function operation001()
{
  this.value = 5;
}

operation001.prototype.add = function()
{
  this.value++;
}

var object1 = new operation001 ();
console.log (object1.value); // object1.value = 5
/*
function operation2 (fnc)
{
	fnc.value = 6;
}
operation2(object1);
*/
object1.add();
console.log (object1.value); // object1.value = 6
function operation002 (add)
{
	add(); // calls the operation being passed in
}
operation002 (object1.add);
console.log (object1.value); // sorry, still just 6