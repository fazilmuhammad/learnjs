let ModuleWide_object = { column1 : 10 }

function update_column (operand1)
{ // read-only operand;
  let local_object = Object.assign ({}, operand1); 
  console.log (local_object.column1);
  local_object.column1 = 20;
  console.log (local_object.column1);
}

update_column (ModuleWide_object);
console.log (ModuleWide_object.column1); // value unchanged