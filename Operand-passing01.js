let myObj = { value:10 }

function updateValue (objRef)
{ //creates a new object reference and assigns objRef properties to //the object
    let copy = Object.assign ({},objRef); 
    console.log (copy.value);
    copy.value = 20;
    console.log (copy.value);
}

updateValue (myObj);
console.log (myObj.value);