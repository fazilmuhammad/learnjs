// Program Simple22 in 'Programming Theory - Basic' book

function operation2 (a)
{
  console.log ('In called operation, value of a is ', a);
  a = a + 1;  // increase the value of a
  console.log ('In called operation, value of a is ', a);
}

// void main()

  var b = 2;
  console.log ('Value of actual operand before calling: ', b);
  operation2(b);
  console.log ('Value of actual operand after calling: ', b);
