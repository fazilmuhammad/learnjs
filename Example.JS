Selamat pagi rekans, saya lanjutkan tentang ORM, EA (Enterprise Architecture) invensi saya.

Ringkasnya, ORM terdiri atas 8 bagian, tapi saya paparkan 6 saja dalam tahap awal ini
1. Organization
2. Business-process
3. Hardware
4. Software
5. Information
6. Bandwidth

Hal amat mendasar untuk kesuksesan menerapkan EA ini adalah KEBERANIAN untuk ENYAHKAN SINONIM. Sebagai contoh, enyahkan sinonim Procedure, SOP (Standard Operating Procedure), Activity, Task, dan banyak sinonim lain. Dalam pengamatan saya: kegagalan banyak orang dalam belajar adalah KETAKUTAN atau KETIDAKMAUAN ENYAHKAN SINONIM.

'Homework' Bpk/Ibu: save as sebuah doc dari proyek Sistem Informasi. Ganti semua sinonim di atas (dan yg lain bila ada) dengan Business-process (atau proses-bisnis, dlm bhs Indonesia).

Saya tutup dgn hal ini. Pak Aryo (seorang anggota Aisindo) yang dorong saya utk jelaskan invensi saya. Saya terima, dan itu sebabnya saya lakukan ini.

Dengan catatan, bahwa berhubung job utama saya dan bisnis corp tempat saya bekerja bukan EA, serta saya sendiri sdh punya cukup beban; saya tidak bisa jelaskan detail secara berkala via grup ini. Kalau saya paksakan di dalam jam kerja, tdk enak ke employer. Kalau di luar jam kerja, tdk enak ke keluarga; sdh kepala 5, tdk mau ngoyo.

Yg bisa saya lakukan kalau rekan2 undang saya jelaskan cara terapkan, saya bisa datang dan detailkan. Tulisan2 itu boleh di-share di dalam organisasi sendiri atau keluar; saya ok. Dan utk supervised practice, welcome. Supervised practice ini mau di-share, gak masalah. 

Sekian dulu, terimakasih; selamat bekerja.

Aksi saya tergantung rekan2 di grup itu; kalau pasif saja, saya tdk bisa teruskan. Tapi kalau diundang utk datang, saya bisa prepare tulisan; tulisannya bisa di-copy, tdk apa-apa. Yg penting ada justifikasi.