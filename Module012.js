function operation04 (operand1 = 1)
{
  console.log (operand1);
}

function operation05 (operand1, operand2 = 'a')
{
  console.log (operand1);
  console.log (operand2);
}

// void main()
operation04 ();
operation04 (5);
operation05 (false);
operation05 (2, 'b');