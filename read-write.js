function operation1()
{
	this.value = 5;
}

function operation2 (an_operand)
{
	an_operand.value = an_operand.value + 1;
}

function main()
{
  var object1 = new operation1();
  alert (object1.value); // object1.value = 5
  operation2 (object1);
  alert (object1.value); // object1.value is now equal to 6
}