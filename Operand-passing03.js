let myArray =[1,2,3];

function addToArray (arrayRef, value)
{ // collection-object is passed-by-address
    arrayRef.push (value); // Strange, treated like stack
} // the value may change after exiting an operation-body

console.log (myArray);
addToArray (myArray,4);
console.log (myArray);