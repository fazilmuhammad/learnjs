  var a = 1;
  switch (a) 
  { 
    case 1: console.log ('The value of a is 1');              // branch 1
    case 2: console.log ('The value of a is not 2');         // branch 2
    default: console.log ('The value is neither 1 nor 2');  // branch 3
  }