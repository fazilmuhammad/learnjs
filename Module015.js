  console.log ('Calling unary operation !: ');
  console.log ('!false > ', !false, '!null > ', !null, '!true > ', !true);
  console.log ('Calling binary operation ||: ');
  console.log ('false && null yields ', false && null);
  console.log (' null && null yields ', true && null);
  console.log (' true && null yields ', null && null);
  console.log ('false || null yields ', false || null);
  console.log (' null || null yields ', null || null);
  console.log (' true || null yields ', true || null);