https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/

var http = require('http');

http.createServer
(
 function(request, response) 
 {
  var headers = request.headers;
  var method = request.method;
  var url = request.url;
  var body = [];
  request
  .on
  ('error', function(err) 
   { console.error(err); }
  )
  .on
  ('data', function(chunk) 
   { body.push(chunk); }
  )
  .on
  (
   'end', function() 
   {
     body = Buffer.concat(body).toString();
     // At this point, we have the headers, operations, address and 
     // operation-body. You can do whatever to respond to this request.
   }
  );
 }
).listen(8080); // Activates this server, listening on port 8080.